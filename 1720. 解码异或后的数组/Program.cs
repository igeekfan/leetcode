﻿using System;

namespace _1720._解码异或后的数组
{
    /// <summary>
    /// https://leetcode.cn/problems/decode-xored-array/
    /// </summary>
    public class Solution
    {
        /// <summary>
        /// b^b=1  a^1=a
        ///【a^b = c ， a^b^b = a， 即 c^b=a 同理 c^a =b】
        /// </summary>
        /// <param name="encoded"></param>
        /// <param name="first"></param>
        /// <returns></returns>
        public int[] Decode(int[] encoded, int first)
        {
            int n = encoded.Length + 1;
            int[] arr = new int[n];
            arr[0] = first;
            for (var i = 1; i < n; i++)
            {
                arr[i] = arr[i - 1] ^ encoded[i - 1];
            }
            return arr;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {

            var arrs = new Solution().Decode(new int[] { 1, 2, 3 }, 1);

            Print(arrs);

            arrs = new Solution().Decode(new int[] { 6, 2, 7, 3 }, 4);

            Print(arrs);

        }

        static void Print(int[] arrs)
        {
            foreach (var item in arrs)
            {
                Console.Write(item);
            }
            Console.WriteLine();
        }
    }
}
