﻿using System;

namespace _237._删除链表中的节点
{
    /// <summary>
    /// 删除链表的倒数第N个节点
    /// </summary>
    public class ListNode
    {
        public int val;
        public ListNode next;
        public ListNode(int x) { val = x; }
    }
    public class Solution
    {
        public void DeleteNode(ListNode node)
        {
            node.val = node.next.val;
            node.next = node.next.next;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            ListNode node = new ListNode(4)
            {
                next = new ListNode(5)
                {
                    next = new ListNode(1)
                    {
                        next = new ListNode(9)
                        {

                        }
                    }
                }
            };

            new Solution().DeleteNode(node.next);


            Print(node);
        }


        static void Print(ListNode node)
        {
            if (node.next != null)
            {
                Print(node.next);
            }

            Console.WriteLine(node.val);
        }
    }
}
