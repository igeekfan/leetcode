﻿using System;
using System.Text;

namespace _12._整数转罗马数字
{
    /// <summary>
    /// https://leetcode.cn/problems/integer-to-roman/
    /// </summary>
    public class Solution
    {
        public string IntToRoman(int num)
        {
            string[] ro_strings = new string[13] { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" };
            int[] nums = new int[13] { 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 };

            StringBuilder rs = new StringBuilder();

            for (int i = 0; i < nums.Length;)
            {
                if (num - nums[i] >= 0)
                {
                    num = num - nums[i];
                    rs.Append(ro_strings[i]);
                    if (num < nums[i])
                    {
                        i++;
                    }
                }
                else
                {
                    i++;
                }
            }
            return rs.ToString();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(new Solution().IntToRoman(3));
            Console.WriteLine(new Solution().IntToRoman(4));
            Console.WriteLine(new Solution().IntToRoman(9));
            Console.WriteLine(new Solution().IntToRoman(58));
            Console.WriteLine(new Solution().IntToRoman(1994));
        }
    }
}
