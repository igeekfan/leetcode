﻿using System;
using System.Collections.Generic;
using Xunit;
using Xunit.Abstractions;

namespace _001.日刷
{
    public class _9_回文数
    {
        /// <summary>
        /// https://leetcode.cn/problems/palindrome-number/
        /// 执行用时：52 ms, 在所有 C# 提交中击败了77.06%的用户
        ///内存消耗：29.5 MB, 在所有 C# 提交中击败了32.12%的用户
        ///通过测试用例：11510 / 11510
        /// </summary>
        public class Solution
        {
            public bool IsPalindrome(int x)
            {
                if (x < 0) return false;
                if (x < 10) return true;
                string xT = x.ToString();
                int begin = 0;
                int end = xT.Length - 1;
                for (; begin < end; begin++, end--)
                {
                    if (xT[begin] != xT[end])
                    {
                        return false;
                    }
                }
                return true;
            }

            /// <summary>
            /// 进阶：你能不将整数转为字符串来解决这个问题吗？
            /// 执行用时：56 ms, 在所有 C# 提交中击败了61.13%的用户
            /// 内存消耗：28.7 MB, 在所有 C# 提交中击败了82.64%的用户
            /// </summary>
            /// <param name="x"></param>
            /// <returns></returns>
            public bool IsPalindrome2(int x)
            {
                if (x < 0) return false;
                if (x < 10) return true;

                long sum = 0;
                int z = x;
                while (z != 0)
                {
                    int y = z % 10;
                    sum = sum * 10 + y;
                    if (sum > int.MaxValue) return false;
                    z = z / 10;
                }

                if (sum != x) return false;

                return true;
            }
        }


        ITestOutputHelper output;
        public _9_回文数(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Test1()
        {
            Solution S = new();

            output.WriteLine("" + S.IsPalindrome(121));
            output.WriteLine("" + S.IsPalindrome(-121));
            output.WriteLine("" + S.IsPalindrome(10));
            output.WriteLine("" + S.IsPalindrome(-101));
            output.WriteLine("");

            output.WriteLine("" + S.IsPalindrome2(121));
            output.WriteLine("" + S.IsPalindrome2(-121));
            output.WriteLine("" + S.IsPalindrome2(10));
            output.WriteLine("" + S.IsPalindrome2(-101));
        }


    }
}
