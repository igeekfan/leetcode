﻿using System;
using System.Collections.Generic;
using Xunit;
using Xunit.Abstractions;

namespace _001.日刷
{
    public class _1614_括号的最大嵌套深度
    {
        /// <summary>
        /// https://leetcode.cn/problems/maximum-nesting-depth-of-the-parentheses/
        /// </summary>
        public class Solution
        {
            public int MaxDepth(string s)
            {
                return 1;
            }
        }


        ITestOutputHelper output;
        public _1614_括号的最大嵌套深度(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Test1()
        {
            Solution S = new();

            output.WriteLine("" + S.MaxDepth("(1+(2*3)+((8)/4))+1"));
            output.WriteLine("" + S.MaxDepth("(1)+((2))+(((3)))"));
            output.WriteLine("" + S.MaxDepth("1+(2*3)/(2-1)"));
            output.WriteLine("" + S.MaxDepth("1"));

        }


    }
}
