﻿using System;
using System.Collections.Generic;
using Xunit;
using Xunit.Abstractions;

namespace _001.日刷
{
    public class _16_最接近的三数之和
    {
        /// <summary>
        /// https://leetcode.cn/problems/3sum-closest/
        /// </summary>
        public class Solution
        {
            public int ThreeSumClosest(int[] nums, int target)
            {
                return nums[target];
            }
        }


        ITestOutputHelper output;
        public _16_最接近的三数之和(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Test1()
        {
            Solution S = new();

            output.WriteLine("" + S.ThreeSumClosest(new int[] { -1, 2, 1, -4 }, 1));
            output.WriteLine("" + S.ThreeSumClosest(new int[] { 0, 0, 0 }, 1));

        }


    }
}
