﻿using System;
using System.Collections.Generic;
using Xunit;
using Xunit.Abstractions;

namespace _001.日刷
{
    public class _17_电话号码的字母组合
    {
        /// <summary>
        /// https://leetcode.cn/problems/letter-combinations-of-a-phone-number/
        /// </summary>
        public class Solution
        {
            Dictionary<char, string> dictMap = new Dictionary<char, string>
            {
                {'2',"abc" },
                {'3',"def" },
                {'4',"ghi" },
                {'5',"jkl" },
                {'6',"mno" },
                {'7',"pqrs" },
                {'8',"tuv" },
                {'9',"wxyz" },
            };
            public IList<string> LetterCombinations(string digits)
            {
                if (string.IsNullOrEmpty(digits)) return new List<string>();

                var result = new List<string>();

                DFS(result, digits, 0, "");
                return result;

            }

            public void DFS(List<string> result, string digits, int index, string s)
            {
                if (index == digits.Length)
                {
                    result.Add(s);
                    return;
                }

                foreach (var c in dictMap[digits[index]])
                {
                    DFS(result, digits, index + 1, s + c);
                }
                return;
            }
        }


        ITestOutputHelper output;
        public _17_电话号码的字母组合(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Test1()
        {
            Solution S = new();

            Print(S.LetterCombinations("23"));
            Print(S.LetterCombinations(""));
            Print(S.LetterCombinations("2"));
        }


        void Print(IList<string> arr)
        {
            foreach (string item in arr)
            {
                output.WriteLine(item);
            }
            output.WriteLine("");
        }
    }
}
