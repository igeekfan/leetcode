﻿using System;
using System.Collections;
using System.Collections.Generic;
using Xunit;
using Xunit.Abstractions;

namespace _001.日刷
{
    public class _89_格雷编码
    {
        /// <summary>
        /// https://leetcode.cn/problems/3sum-closest/
        /// </summary>
        public class Solution
        {
            public IList<int> GrayCode(int n)
            {

                List<BitArray> list = new List<BitArray>();


                for (int i = 0; i < Math.Pow(2, n); i++)
                {
                    BitArray bitArray = new BitArray(BitConverter.GetBytes(i));
                    list.Add(bitArray);
                }

                List<int> result = new List<int>();


                return result;
            }
        }


        ITestOutputHelper output;
        public _89_格雷编码(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Test1()
        {
            Solution S = new Solution();

            Print(S.GrayCode(2));
            output.WriteLine("");
            Print(S.GrayCode(1));

        }
        void Print(IList<int> list)
        {
            foreach (var item in list)
            {
                output.WriteLine("" + item);
            }
        }


    }
}
