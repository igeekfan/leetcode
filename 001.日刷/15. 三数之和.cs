﻿using System;
using System.Collections.Generic;
using Xunit;
using Xunit.Abstractions;

namespace _001.日刷
{
    public class _15_三数之和
    {
        /// <summary>
        /// https://leetcode.cn/problems/3sum/
        /// </summary>
        public class Solution
        {
            public IList<IList<int>> ThreeSum(int[] nums)
            {
                return null;
            }
        }


        ITestOutputHelper output;
        public _15_三数之和(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Test1()
        {
            Solution S = new();

            Print(S.ThreeSum(new int[] { -1, 0, 1, 2, -1, -4 }));
        }

        private void Print(IList<IList<int>> list)
        {
            foreach (var li in list)
            {
                foreach (var item in li)
                {
                    output.WriteLine("" + item);
                }
            }
        }


    }
}
