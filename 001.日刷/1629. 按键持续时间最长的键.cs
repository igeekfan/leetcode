﻿using System;
using System.Collections.Generic;
using Xunit;
using Xunit.Abstractions;

namespace _001.日刷
{
    public class _1629_按键持续时间最长的键
    {
        /// <summary>
        /// https://leetcode.cn/problems/slowest-key/
        /// </summary>
        public class Solution
        {
            /// <summary>
            /// 2022-1-9
            ///执行用时：112 ms, 在所有 C# 提交中击败了50.00%的用户
            ///内存消耗：39.7 MB, 在所有 C# 提交中击败了42.86%的用户通过测试用例：
            ///109 / 109
            /// </summary>
            /// <param name="releaseTimes"></param>
            /// <param name="keysPressed"></param>
            /// <returns></returns>
            public char SlowestKey(int[] releaseTimes, string keysPressed)
            {
                int max = releaseTimes[0];
                char key = keysPressed[0];

                for (int i = 1; i < releaseTimes.Length; i++)
                {
                    if (max <= releaseTimes[i] - releaseTimes[i - 1])
                    {
                        if (max == releaseTimes[i] - releaseTimes[i - 1] && key >= keysPressed[i])
                        {

                        }
                        else
                        {
                            key = keysPressed[i];
                        }
                        max = releaseTimes[i] - releaseTimes[i - 1];
                    }
                }

                return key;
            }
        }


        ITestOutputHelper output;
        public _1629_按键持续时间最长的键(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Test1()
        {
            Solution S = new();

            output.WriteLine("" + S.SlowestKey(new int[] { 9, 29, 49, 50 }, "cbcd"));
            output.WriteLine("" + S.SlowestKey(new int[] { 12, 23, 36, 46, 62 }, "spuda"));
            output.WriteLine("" + S.SlowestKey(new int[] { 1, 2 }, "ba"));

        }


    }
}
