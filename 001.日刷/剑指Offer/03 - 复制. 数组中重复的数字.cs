﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace _001.日刷.剑指Offer
{

    public class _03_数组中重复的数字
    {
        public class Solution
        {
            public int FindRepeatNumber(int[] nums)
            {
                for (var i = 0; i < nums.Length - 1; i++)
                {
                    int te = nums[i];
                    for (var j = i + 1; j < nums.Length; j++)
                    {
                        if (te == nums[j])
                        {
                            return te;
                        }
                    }
                }
                return 0;
            }
        }

        ITestOutputHelper output;
        public _03_数组中重复的数字(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Test1()
        {
            Solution rSolution = new Solution();
            var nums = new int[] { 2, 3, 1, 0, 2, 5, 3 };
            var num = rSolution.FindRepeatNumber(nums);
            output.WriteLine(num+"");
        }
    }
}
