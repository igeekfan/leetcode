﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace _001.日刷.剑指Offer
{

    public class _53_II_0_n1中缺失的数字
    {
        public class Solution
        {
            public int MissingNumber(int[] nums)
            {
                if (nums.Length == 0) return 0;
                if (nums[0] != 0) return 0;
                int te = 0;

                for (var i = 0; i < nums.Length - 1; i++)
                {
                    if (nums[i] + 1 != nums[i + 1])
                    {
                        return nums[i] + 1;
                    }
                }
                return nums[nums.Length - 1] + 1;
            }
        }

        ITestOutputHelper output;
        public _53_II_0_n1中缺失的数字(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Test1()
        {
            Solution rSolution = new Solution();
            var nums = new int[] { 0, 1, 3 };
            var array = rSolution.MissingNumber(nums);
            output.WriteLine(array + "");

            nums = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 9 };
            array = rSolution.MissingNumber(nums);
            output.WriteLine(array + "");


            nums = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            array = rSolution.MissingNumber(nums);
            output.WriteLine(array + "");
        }
    }
}
