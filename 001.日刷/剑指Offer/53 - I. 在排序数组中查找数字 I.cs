﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace _001.日刷.剑指Offer
{

    public class _53_I在排序数组中查找数字_I
    {
        public class Solution
        {
            public int Search(int[] nums, int target)
            {
                int num = 0;
                for (int i = 0; i < nums.Length; i++)
                {
                    if (nums[i] == target)
                    {
                        ++num;
                    }          
                }

                return num;
            }
        }

        ITestOutputHelper output;
        public _53_I在排序数组中查找数字_I(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Test1()
        {
            Solution rSolution = new Solution();
            var nums = new int[] { 5, 7, 7, 8, 8, 10 };
            var num = rSolution.Search(nums, 8);
            output.WriteLine(num+"");

             num = rSolution.Search(nums, 6);
            output.WriteLine(num + "");
        }
    }
}
