﻿using System;
using System.Collections;
using System.Collections.Generic;
using Xunit;
using Xunit.Abstractions;

namespace _001.日刷.剑指Offer
{
    /// <summary>
    /// https://leetcode.cn/problems/bao-han-minhan-shu-de-zhan-lcof/
    /// 剑指 Offer _30包含min函数的栈
    /// </summary>
    public class _30包含min函数的栈
    {

        public class MinStack
        {
            Stack<int> StackX;
            Stack<int> StackY;


            /** initialize your data structure here. */
            public MinStack()
            {
                StackX = new Stack<int>();
                StackY = new Stack<int>();
            }

            private int min = int.MaxValue;
            public void Push(int x)
            {
                if (min >= x)
                {
                    min = x;
                    StackY.Push(x);
                }
                else
                {
                    StackY.Push(min);
                }
                StackX.Push(x);

            }

            public void Pop()
            {
                StackX.Pop();
                StackY.Pop();
                if (StackY.Count == 0)
                {
                    min = int.MaxValue;
                }
                else
                {
                    min = StackY.Peek();
                }
            }

            public int Top()
            {
                return StackX.Peek();
            }

            public int Min()
            {
                return StackY.Peek();
            }
        }


        ITestOutputHelper output;
        public _30包含min函数的栈(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Test1()
        {
            MinStack minStack = new MinStack();
            minStack.Push(-10);
            minStack.Push(14);
            var min = minStack.Min();
            Console.WriteLine(min);
            min = minStack.Min();
            Console.WriteLine(min);
            minStack.Push(-20);
            min = minStack.Min();
            Console.WriteLine(min);
            min = minStack.Min();
            Console.WriteLine(min);
            min = minStack.Top();
            Console.WriteLine(min);
            min = minStack.Min();
            Console.WriteLine(min);
            minStack.Pop();
            Console.WriteLine();
            minStack.Push(10);
            Console.WriteLine();
            minStack.Push(-7);
            Console.WriteLine();
            min = minStack.Min();
            Console.WriteLine(min);
        }

    }
}
