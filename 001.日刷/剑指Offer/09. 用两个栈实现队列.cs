﻿using System.Collections;
using System.Collections.Generic;
using Xunit;
using Xunit.Abstractions;

namespace _001.日刷.剑指Offer
{
    /// <summary>
    /// 剑指 Offer 09. 用两个栈实现队列
    /// </summary>
    public class _09_用两个栈实现队列
    {
        /// <summary>
        /// https://leetcode.cn/problems/yong-liang-ge-zhan-shi-xian-dui-lie-lcof/
        /// </summary>
        public class CQueue
        {
            private Stack<int> stack1 = new Stack<int>();
            private Stack<int> stack2 = new Stack<int>();
            public CQueue()
            {

            }

            public void AppendTail(int value)
            {
                stack1.Push(value);
            }

            public int DeleteHead()
            {
                if (stack1.Count == 0) return -1;

                while (stack1.Count > 0)
                {
                    int value = stack1.Pop();
                    stack2.Push(value);
                }

                int headValue = stack2.Pop();
                while (stack2.Count > 0)
                {
                    int value = stack2.Pop();
                    stack1.Push(value);
                }

                return headValue;
            }
        }


        ITestOutputHelper output;
        public _09_用两个栈实现队列(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Test1()
        {
            CQueue obj = new CQueue();
            int value = 0;
            obj.AppendTail(value);
            int param_2 = obj.DeleteHead();
            output.WriteLine(param_2 + "");
        }

    }
}
