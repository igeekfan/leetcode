﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace _001.日刷.剑指Offer
{
    public class _58_II_左旋转字符串
    {
        public class Solution
        {
            public string ReverseLeftWords(string s, int n)
            {
                StringBuilder r = new StringBuilder();

                char[] chas = s.ToCharArray();

                for (var j = 0; j < n; j++)
                {
                    char temp = chas[0];
                    for (var i = 0; i < chas.Length - 1; i++)
                    {
                        chas[i] = chas[i + 1];
                    }
                    chas[chas.Length - 1] = temp;
                }

                return new string(chas);
            }
        }

        ITestOutputHelper output;
        public _58_II_左旋转字符串(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Test1()
        {
            Solution rSolution = new Solution();
            var r = rSolution.ReverseLeftWords("abcdefg", 2);
            output.WriteLine(r);
        }
    }
}
