﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace _001.日刷.剑指Offer
{
  
	public class _05_替换空格
	{
        public class Solution
        {
            public string ReplaceSpace(string s)
            {
                StringBuilder r = new StringBuilder();
                foreach (var t in s)
                {
                    if (t == ' ')
                    {
                        r.Append("%20");
                    }
                    else
                    {
                        r.Append(t);
                    }
                }
                return r.ToString();
            }
        }

		ITestOutputHelper output;
        public _05_替换空格(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Test1()
        {
            Solution rSolution = new Solution();
            var array = rSolution.ReplaceSpace("We are happy.");
            output.WriteLine(array);
        }
    }
}
