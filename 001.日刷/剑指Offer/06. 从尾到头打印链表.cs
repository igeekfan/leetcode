﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace _001.日刷.剑指Offer
{
	public class ListNode
	{
		public int val;
		public ListNode next;
		public ListNode(int x) { val = x; }
	}

	public class Solution
	{
		List<int> list = new List<int>();

		public int[] ReversePrint(ListNode head)
		{
			while (head != null)
			{
				var val = head.val;
				list.Add(val);
				head = head.next;
			}

            list.Reverse();

            return list.ToArray();
		}
	}
	public class _06从尾到头打印链表
	{
		ITestOutputHelper output;
		public _06从尾到头打印链表(ITestOutputHelper output)
		{
			this.output = output;
		}

		[Fact]
		public void Test1()
		{
			var listnode = new ListNode(1)
			{
				next = new ListNode(3)
				{
					next = new ListNode(2)
				}
			};

			Solution rSolution = new Solution();
			var array = rSolution.ReversePrint(listnode);

			foreach (var item in array)
			{
				output.WriteLine(item + "");
			}
		}

	}
}
