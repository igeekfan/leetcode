﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace _001.日刷.剑指Offer
{

    public class _50_第一个只出现一次的字符
    {
        public class Solution
        {
            public char FirstUniqChar(string s)
            {
                int[] numIntss = new int[26];

                for (int i = 0; i < s.Length; i++)
                {
                    numIntss[s[i] - 'a']++;
                }

                for (int i = 0; i < s.Length; i++)
                {
                    if (numIntss[s[i] - 'a'] == 1)
                    {
                        return s[i];
                    }
                }

                return ' ';
            }
        }

        ITestOutputHelper output;
        public _50_第一个只出现一次的字符(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Test1()
        {
            Solution rSolution = new Solution();
            var num = rSolution.FirstUniqChar("abaccdeff");
            output.WriteLine(num + "");
            num = rSolution.FirstUniqChar("");
            output.WriteLine(num + "");
            num = rSolution.FirstUniqChar("leetcode");
            output.WriteLine(num + "");
        }
    }
}
