﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Formatters;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Xunit;
using Xunit.Abstractions;

namespace _001.日刷.剑指OfferII
{
    public class _033变位词组
    {
        public class Solution
        {
            public IList<IList<string>> GroupAnagrams(string[] strs)
            {
                if (strs.Length == 0)
                {
                    return new List<IList<string>>();
                }
                var re = new List<IList<string>>();

                string[] copyStrs = new string[strs.Length];

                for (int i = 0; i < strs.Length; i++)
                {
                    var chars = strs[i].ToCharArray();

                    Array.Sort(chars);
                    copyStrs[i] = new string(chars);
                }

                int x = 0;
                int[] isuse = new int[strs.Length];
                ;
                for (int i = 0; i < strs.Length; i++)
                {
                    if (isuse[i] == 1)
                    {
                        continue;
                    }
                    var chars = copyStrs[i];
                    re.Add(new List<string>());
                    re[x].Add(strs[i]);
                    for (int j = i + 1; j < strs.Length; ++j)
                    {
                        if (chars == copyStrs[j] && isuse[j] == 0)
                        {
                            isuse[j] = 1;
                            re[x].Add(strs[j]);
                        }
                    }
                    x++;

                }
                return re;
            }
        }

        ITestOutputHelper output;
        public _033变位词组(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Test1()
        {

            Solution rSolution = new Solution();

            string[] strs = new String[]
            {
                "eat", "tea", "tan", "ate", "nat", "bat"
            };

            var r = rSolution.GroupAnagrams(strs);
            foreach (var a in r)
            {
                foreach (var x in a)
                {
                    output.WriteLine(x);
                }
                output.WriteLine("============");
            }

            output.WriteLine("=AAAAAAAAAAAAAAAAAAAAAAAAAAAA=");
            strs = new String[]
            {
                "", "b"
            };

            r = rSolution.GroupAnagrams(strs);
            foreach (var a in r)
            {
                foreach (var x in a)
                {
                    output.WriteLine(x);
                }
                output.WriteLine("============");
            }

        }
    }
}
