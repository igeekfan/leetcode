﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace _001.日刷.剑指OfferII
{
    public class _003前n个数字二进制中1的个数
    {
        public class Solution
        {
            public int Count(int x)
            {
                int num = 0;

                int n = x;


                while (n != 0)
                {
                    int p = n % 2;
                    n = n / 2;
                    if (p == 1)
                    {
                        num++;
                    }
                }
                return num;
            }
            public int[] CountBits(int n)
            {
                var x = new List<int>();

                for (var i = 0; i <=n; i++)
                {
                    x.Add(Count(i));
                }

                return x.ToArray();
            }
        }

        ITestOutputHelper output;
        public _003前n个数字二进制中1的个数(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Test1()
        {
            Solution rSolution = new Solution();
            var r = rSolution.CountBits(2);
            foreach (var x in r)
            {
                output.WriteLine(x + "");
            }
        }
    }
}
