﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Formatters;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace _001.日刷.剑指OfferII
{
    public class _004只出现一次的数字
    {
        public class Solution
        {
            public int SingleNumber(int[] nums)
            {
                int r = 0;
                Dictionary<int,int>dict=new Dictionary<int,int>();

                for (int i = 0; i < nums.Length; i++)
                {
                    if (dict.ContainsKey(nums[i]))
                    {
                        dict[nums[i]]++;
                    }
                    else
                    {
                        dict[nums[i]]=1;
                    }
                }

                foreach (var dic in dict)
                {
                    if (dic.Value != 0 && dic.Value != 3)
                    {
                        return dic.Key;
                    }
                }
               
                return r;
            }
        }

        ITestOutputHelper output;
        public _004只出现一次的数字(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Test1()
        {
            Solution rSolution = new Solution();
            var r = rSolution.SingleNumber(new int[] { 0, 1, 0, 1, 0, 1, 100 });
            output.WriteLine(r + "");
        }
    }
}
