﻿using System;

namespace _1486._数组异或操作
{
    /// <summary>
    /// https://leetcode.cn/problems/xor-operation-in-an-array/
    /// </summary>
    public class Solution
    {
        /// <summary>
        /// 方法一：模拟
        /// </summary>
        /// <param name="n"></param>
        /// <param name="start"></param>
        /// <returns></returns>
        public int XorOperation(int n, int start)
        {
            int r = start;
            for (int i = 1; i < n; i++)
            {
                r ^= start + 2 * i;
            }
            return r;
        }
        //另外一个题解看不懂
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(new Solution().XorOperation(5, 0));
            Console.WriteLine(new Solution().XorOperation(4, 3));
            Console.WriteLine(new Solution().XorOperation(1, 7));
            Console.WriteLine(new Solution().XorOperation(10, 5));
        }
    }
}
