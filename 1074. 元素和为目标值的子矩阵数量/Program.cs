﻿using System;

namespace _1074._元素和为目标值的子矩阵数量
{
    public class Solution
    {
        public int NumSubmatrixSumTarget(int[][] matrix, int target)
        {
            return 1;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var matrix = new int[][] {
                new int[]{0,1,0},
                new int[]{1,1,1},
                new int[]{ 0,1,0}
            };
            Console.WriteLine(new Solution().NumSubmatrixSumTarget(matrix, 0));

            var matrix2 = new int[][] {
                new int[]{1,-1},
                new int[]{-1,1},
            };
            Console.WriteLine(new Solution().NumSubmatrixSumTarget(matrix2, 0));

            var matrix3 = new int[][] {
                new int[]{904 },
            };
            Console.WriteLine(new Solution().NumSubmatrixSumTarget(matrix3, 0));
        }
    }
}
