﻿using System;

namespace _7._整数反转
{
    /// <summary>
    /// https://leetcode.cn/problems/reverse-integer/
    /// </summary>
    public class Solution
    {
        /// <summary>
        /// -2^31 <= x <= 2^31 - 1
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public int Reverse(int x)
        {
            long y = 0;
            while (x != 0)
            {
                long le = x % 10;
                y = y * 10 + le;
                x /= 10;
            }
            return (y > int.MaxValue || y < int.MinValue) ? 0 : (int)y;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(new Solution().Reverse(123));
            Console.WriteLine(new Solution().Reverse(-123));
            Console.WriteLine(new Solution().Reverse(120));
            Console.WriteLine(new Solution().Reverse(0));
            Console.WriteLine(new Solution().Reverse(1534236469));
            Console.WriteLine(new Solution().Reverse(-2147483648));

        }
    }
}
