﻿using System;
using System.Diagnostics;

namespace _8._字符串转换整数__atoi_
{
    /// <summary>
    /// https://leetcode.cn/problems/string-to-integer-atoi/
    /// </summary>
    public class Solution
    {
        /// <summary>
        /// 1082 / 1082 个通过测试用例
        ///状态：通过
        ///执行用时: 84 ms
        ///内存消耗: 24.7 MB
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public int MyAtoi(string s)
        {
            char[] cc = new char[s.Length];
            bool prefix = true;
            int n = 0;

            if (s.StartsWith(' ') || s.StartsWith('0'))
            {
                for (int i = 0; i < s.Length; i++)
                {
                    if (s[i] == ' ' && prefix)
                    {

                    }
                    else
                    {
                        cc[n++] = s[i];
                        prefix = false;
                    }

                }
            }
            else
            {
                cc = s.ToCharArray();
                n = cc.Length;
            }


            if (n == 0) return 0;
            bool negative = cc[0] == '-' ? true : false;
            if (cc[0] != '-' && cc[0] != '+')
            {
                if (!char.IsDigit(cc[0]))
                {
                    return 0;
                }
            }
            double sum = 0;
            if (!negative)
            {
                if (char.IsDigit(cc[0]))
                {
                    sum = cc[0] - '0';
                }
            }
            for (int i = 1; i < n; i++)
            {
                if (char.IsDigit(cc[i]))
                {
                    sum = sum * 10 + cc[i] - '0';
                    continue;
                }
                break;
            }

            if (negative)
            {
                return -sum < int.MinValue ? int.MinValue : -(int)sum;
            }
            else
            {
                return sum > int.MaxValue ? int.MaxValue : (int)sum;
            }
        }
        public int MyAtoi2(string s)
        {
            char[] cc = new char[s.Length];
            bool prefix = true;
            int n = 0;

            if (s.StartsWith(' ') || s.StartsWith('0'))
            {
                for (int i = 0; i < s.Length; i++)
                {
                    if (s[i] == ' ' && prefix)
                    {

                    }
                    else
                    {
                        cc[n++] = s[i];
                        prefix = false;
                    }
                }
            }
            else
            {
                cc = s.ToCharArray();
                n = cc.Length;
            }

            if (n == 0) return 0;
            if (cc[0] != '-' && cc[0] != '+' && !char.IsDigit(cc[0]))
            {
                return 0;
            }
            bool negative = cc[0] == '-' ? true : false;
            long sum = 0;
            if (!negative)
            {
                if (char.IsDigit(cc[0]))
                {
                    sum = cc[0] - '0';
                }
            }
            for (int i = 1; i < n; i++)
            {
                if (char.IsDigit(cc[i]))
                {
                    sum = sum * 10 + cc[i] - '0';

                    if (!negative && sum > int.MaxValue)
                    {
                        sum = int.MaxValue;
                        break;
                    }
                    if (negative && sum > 1L + int.MaxValue)
                    {
                        sum = int.MaxValue + 1L;
                        break;
                    }
                    continue;
                }
                break;
            }

            return negative ? (int)-sum : (int)sum;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine(new Solution().MyAtoi("  4 2"));
            Stopwatch sp = new Stopwatch();
            sp.Start();
            Console.WriteLine(new Solution().MyAtoi("9223372036854775808"));
            Console.WriteLine(new Solution().MyAtoi("+1"));
            Console.WriteLine(new Solution().MyAtoi("4 2"));
            Console.WriteLine(new Solution().MyAtoi(".1"));
            Console.WriteLine(new Solution().MyAtoi("0032"));
            Console.WriteLine(new Solution().MyAtoi("42"));
            Console.WriteLine(new Solution().MyAtoi("   -42"));
            Console.WriteLine(new Solution().MyAtoi("4193 with words"));
            Console.WriteLine(new Solution().MyAtoi("words and 987"));
            Console.WriteLine(new Solution().MyAtoi("-91283472332"));
            sp.Stop();
            Console.WriteLine(sp.ElapsedMilliseconds + "ms");
            Console.WriteLine();

            sp.Start();
            Console.WriteLine(new Solution().MyAtoi2("9223372036854775808"));
            Console.WriteLine(new Solution().MyAtoi2("+1"));
            Console.WriteLine(new Solution().MyAtoi2("4 2"));
            Console.WriteLine(new Solution().MyAtoi2(".1"));
            Console.WriteLine(new Solution().MyAtoi2("0032"));
            Console.WriteLine(new Solution().MyAtoi2("42"));
            Console.WriteLine(new Solution().MyAtoi2("   -42"));
            Console.WriteLine(new Solution().MyAtoi2("4193 with words"));
            Console.WriteLine(new Solution().MyAtoi2("words and 987"));
            Console.WriteLine(new Solution().MyAtoi2("-91283472332"));
            sp.Stop();
            Console.WriteLine(sp.ElapsedMilliseconds + "ms");
        }
    }
}
