﻿using Microsoft.VisualBasic;
using System;

namespace _242._有效的字母异位词
{
    public class Solution
    {
        /// <summary>
        /// 方法一：排序
        /// </summary>
        /// <param name="s"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        public bool IsAnagram(string s, string t)
        {
            if (s.Length != t.Length) return false;

            char[] s1 = s.ToCharArray();
            char[] t1 = t.ToCharArray();
            Array.Sort(s1);
            Array.Sort(t1);

            return new string(s1) == new string(t1);

        }

        /// <summary>
        /// 方法二：哈希表
        /// </summary>
        /// <param name="s"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        public bool IsAnagram2(string s, string t)
        {
            if (s.Length != t.Length) return false;

            int[] nums = new int[26];
            for (int i = 0; i < s.Length; i++)
            {
                nums[s[i] - 'a']++;
            }
            for (int i = 0; i < t.Length; i++)
            {
                nums[t[i] - 'a']--;
                if(nums[t[i] - 'a'] < 0)
                {
                    return false;
                }
            }
            return true;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(new Solution().IsAnagram("anagram", "nagaram"));
            Console.WriteLine(new Solution().IsAnagram("rat", "car"));

            Console.WriteLine(new Solution().IsAnagram2("anagram", "nagaram"));
            Console.WriteLine(new Solution().IsAnagram2("rat", "car"));
        }
    }
}
