﻿using System;

namespace _104._二叉树的最大深度
{
    /// <summary>
    /// https://leetcode.cn/problems/maximum-depth-of-binary-tree/solution/
    /// </summary>
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    public class Solution
    {
        /// <summary>
        /// DFS 深度优先搜索
        /// </summary>
        /// <param name="root"></param>
        /// <returns></returns>
        public int MaxDepth(TreeNode root)
        {
            if(root == null)
            {
                return 0;
            }
            return 1 + Math.Max(MaxDepth(root.left),MaxDepth(root.right));
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            TreeNode treeNodes = new TreeNode(3, new TreeNode(9), new TreeNode(20, new TreeNode(15), new TreeNode(7)));
            Console.WriteLine(new Solution().MaxDepth(treeNodes));
        }
    }
}
