﻿using System;
using System.Text;

namespace _125._验证回文串
{
    public class Solution
    {
        public bool IsPalindrome(string s)
        {
            char[] ss = new char[s.Length];
            int j = 0;
            for (int i = 0; i < s.Length; i++)
            {
                if ((s[i] >= 'a' && s[i] < 'z') || (s[i] >= 'A' && s[i] < 'Z') || (s[i] >= '0' && s[i] <= '9'))
                {
                    ss[j++] = char.ToLower(s[i]);
                }
            }

            for (var i = 0; i < j / 2; i++)
            {
                if (ss[i] != ss[j - 1 - i])
                {
                    return false;
                }
            }
            return true;
        }

        public bool IsPalindrome2(string s)
        {
            StringBuilder ss = new StringBuilder();
            for (int i = 0; i < s.Length; i++)
            {
                if (char.IsLetterOrDigit(s[i]))
                {
                    ss.Append(char.ToLower(s[i]));
                }
            }
            for (var i = 0; i < ss.Length / 2; i++)
            {
                if (ss[i] != ss[ss.Length - 1 - i])
                {
                    return false;
                }
            }
            return true;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(new Solution().IsPalindrome("A man, a plan, a canal: Panama"));
            Console.WriteLine(new Solution().IsPalindrome("race a car"));
            Console.WriteLine(new Solution().IsPalindrome("9,8"));

            Console.WriteLine(new Solution().IsPalindrome2("A man, a plan, a canal: Panama"));
            Console.WriteLine(new Solution().IsPalindrome2("race a car"));
            Console.WriteLine(new Solution().IsPalindrome2("9,8"));
        }
    }
}
