﻿using System;

namespace _387._字符串中的第一个唯一字符
{
    /// <summary>
    /// https://leetcode.cn/problems/first-unique-character-in-a-string/
    /// </summary>
    public class Solution
    {
        public int FirstUniqChar(string s)
        {
    
            int[] nums=new int[26];
            for(int i=0;i<s.Length; i++)
            {
                nums[s[i] - 'a']++;
            }

            for (int i = 0; i < s.Length; i++)
            {
                if (nums[s[i] - 'a'] == 1)
                {
                    return i;
                }
            }
            return -1;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(new Solution().FirstUniqChar("leetcode"));
            Console.WriteLine(new Solution().FirstUniqChar("loveleetcode"));
        }
    }
}
