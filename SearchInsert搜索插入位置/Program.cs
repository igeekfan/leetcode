﻿using System;
using System.Linq;

namespace SearchInsert搜索插入位置
{
    public class Solution
    {
        public int SearchInsert(int[] nums, int target)
        {
            int left = 0, right = nums.Count()-1;

            int mid = 0;
            while (left <= right)
            {
                mid = (right + left) / 2;

                if (nums[mid] == target)
                {
                    return mid;
                }
                else if (nums[mid] < target)
                {
                    left = mid + 1;
                }
                else if (nums[mid] > target)
                {
                    right = mid - 1;
                }
            }

            int i;
            if (nums[0] >=target)
            {
                return 0;
            }
            for (i = 0; i < nums.Count(); i++)
            {
                if (i + 1 < nums.Count() && nums[i] < target && nums[i + 1] > target)
                {
                    
                }
            }

            return i;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            int mid = new Solution().SearchInsert(new int[] { 1, 4, 6, 7, 8, 9}, 6);

            Console.WriteLine(mid);
            Console.WriteLine("Hello World!");
        }
    }
}
