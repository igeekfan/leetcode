﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace _66._加一
{
    /// <summary>
    /// https://leetcode.cn/problems/plus-one/
    /// </summary>
    public class Solution
    {
        public int[] PlusOne(int[] digits)
        {
            int count = digits.Count();
            int carry = 0;

            List<int> rs = new List<int>();
            for (int i = count - 1; i >= 0; i--)
            {
                int y = 0;
                if (i == count - 1)
                {
                    y = digits[i] + 1;
                }
                else
                {
                    y += carry + digits[i];
                }

                rs.Add(y % 10);
                carry = y / 10;
            }

            if (carry != 0)
            {
                rs.Add(carry);
            }

            rs.Reverse();

            return rs.ToArray();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {

            int[] r = new int[10000];
            r = new Solution().PlusOne(new int[] { 1, 2, 3 });

            Print(r);

            r = new Solution().PlusOne(new int[] { 4, 3, 2, 1 });

            Print(r);

            r = new Solution().PlusOne(new int[] { 0 });

            Print(r);

            r = new Solution().PlusOne(new int[] { 9, 9, 9 });

            Print(r);
        }

        static void Print(int[] r)
        {

            foreach (var item in r)
            {
                Console.Write(item);
            }

            Console.WriteLine();

        }
    }
}
