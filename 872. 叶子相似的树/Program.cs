﻿using System;

namespace _872._叶子相似的树
{
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    public class Solution
    {
        public bool LeafSimilar(TreeNode root1, TreeNode root2)
        {

            return false;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            var root1 = new TreeNode();
            var root2 = new TreeNode();
            Console.WriteLine(new Solution().LeafSimilar(root1, root2));
        }
    }
}
