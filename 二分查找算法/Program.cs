﻿using System;
using System.Linq;

namespace 二分查找算法
{
    class Program
    {
        static int SearchInsert(int[] nums, int target)
        {
            int left = 0, right = nums.Count() - 1;

            while (left < right)
            {
                int mid = (right + left) / 2;

                if (nums[mid] == target)
                {
                    return mid;
                }
                else if (nums[mid] < target)
                {
                    left = mid;
                }
                else if (nums[mid] > target)
                {
                    right = mid;
                }
            }

            return -1;
        }

        static void Main(string[] args)
        {


            var mid = SearchInsert(new int[] { 1,2,3,4,5,6,7,8,9,19,22 }, 19);

            Console.WriteLine(mid);



            Console.WriteLine("OVER!");
        }
    }
}
