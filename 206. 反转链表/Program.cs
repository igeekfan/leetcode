﻿using System;

namespace _206._反转链表
{
    /// <summary>
    /// https://leetcode.cn/problems/reverse-linked-list/
    /// </summary>
    public class ListNode
    {
        public int val;
        public ListNode next;
        public ListNode(int val = 0, ListNode next = null)
        {
            this.val = val;
            this.next = next;
        }
    }
    public class Solution
    {
        public ListNode ReverseList(ListNode head)
        {
            ListNode prev = null;
            ListNode current = head;
            while(current != null)
            {
                ListNode next = current.next;
                current.next = prev;
                prev = current;
                current = next;
            }

            return prev;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            ListNode node = new ListNode(1)
            {
                next = new ListNode(2)
                {
                    next = new ListNode(3)
                    {
                        next = new ListNode(4)
                        {
                            next = new ListNode(5)
                            {

                            }
                        }
                    }
                }
            };
            Print(node);
            Console.WriteLine();
            var cc = new Solution().ReverseList(node);
            Print(cc);

            Console.WriteLine();
            node = new ListNode(1)
            {
                next = new ListNode(2)
                {
                }
            };
            Print(node);
            Console.WriteLine();
            cc = new Solution().ReverseList(node);
            Print(cc);
        }


        static void Print(ListNode node)
        {
            Console.Write(node.val);

            if (node.next != null)
            {
                Print(node.next);
            }

        }
    }
}
