﻿namespace _344._反转字符串
{
    /// <summary>
    /// https://leetcode.cn/problems/reverse-string/
    /// </summary>
    public class Solution
    {
        public void ReverseString(char[] s)
        {
            char temp = 'A';

            for (var i = 0; i < s.Length / 2; i++)
            {
                temp = s[i];
                s[i] = s[s.Length - 1 - i];
                s[s.Length - 1 - i] = temp;
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {


            //char[] s = new char[] { 'h', 'e', 'l', 'l', 'o' };
            char[] s = new char[] { 'A', ' ', 'm', 'a', 'n', ',', ' ', 'a', ' ', 'p', 'l', 'a', 'n', ',', ' ', 'a', ' ', 'c', 'a', 'n', 'a', 'l', ':', ' ', 'P', 'a', 'n', 'a', 'm', 'a' };
            for (int i = s.Length - 1; i >= 0; i--)
            {
                System.Console.Write(s[i]);
            }
            System.Console.WriteLine();
            var s1 = new Solution();

            s1.ReverseString(s);

            foreach (var item in s)
            {
                System.Console.Write(item);
            }

            System.Console.WriteLine();
            //s = new char[] { 'H', 'a', 'n', 'n', 'a', 'h' };

            //s1.ReverseString(s);

            s = new char[] { 'a', 'm', 'a', 'n', 'a', 'P', ' ', ':', 'l', 'a', 'n', 'a', 'c', ' ', 'a', ' ', ',', 'n', 'a', 'l', 'p', ' ', 'a', ' ', ',', 'n', 'a', 'm', ' ', 'A' };
            foreach (var item in s)
            {
                System.Console.Write(item);
            }

        }
    }
}
