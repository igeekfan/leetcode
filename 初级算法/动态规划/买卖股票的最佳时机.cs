﻿using Xunit;
using Xunit.Abstractions;

namespace 初级算法.其他
{
    public class 买卖股票的最佳时机
    {
        public class Solution
        {
            /// <summary>
            /// 200 / 211 个通过测试用例 状态：超出时间限制
            /// </summary>
            /// <param name="prices"></param>
            /// <returns></returns>
            public int MaxProfit(int[] prices)
            {
                int[] max = new int[prices.Length];
                for (int i = 0; i < prices.Length; i++)
                {
                    max[i] = 0;

                    for (int j = i + 1; j < prices.Length; j++)
                    {
                        int x = prices[j] - prices[i];
                        if (x > max[i])
                        {
                            max[i] = x;
                        }
                    }

                }

                int maxValue = 0;
                for (int i = 0; i < max.Length; i++)
                {
                    if (maxValue < max[i])
                    {
                        maxValue = max[i];
                    }
                }

                return maxValue;
            }

            public int MaxProfit2(int[] prices)
            {
                int[] max = new int[prices.Length];
                int maxValue = 0;


                return maxValue;
            }
        }
        ITestOutputHelper output;

        public 买卖股票的最佳时机(ITestOutputHelper output)
        {
            this.output = output;
        }


        [Fact]
        public void Test1()
        {

            Solution S = new Solution();

            output.WriteLine("" + S.MaxProfit2(new int[] { 7, 1, 5, 3, 6, 4 }));
            output.WriteLine("" + S.MaxProfit2(new int[] { 7, 6, 4, 3, 1 }));
        }
    }
}
