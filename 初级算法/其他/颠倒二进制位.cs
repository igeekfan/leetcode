﻿
using System;
using System.Collections;
using Xunit;
using Xunit.Abstractions;

namespace 初级算法.其他
{
    public class 颠倒二进制位
    {
        /// <summary>
        /// https://leetcode.cn/leetbook/read/top-interview-questions-easy/xnc5vg/
        /// 输入是一个长度为 32 的二进制字符串
        /// </summary>
        public class Solution
        {
            /// <summary>
            /// 执行用时：52 ms, 在所有 C# 提交中击败了64.07%的用户
            /// 内存消耗：15.7 MB, 在所有 C# 提交中击败了6.87%的用户
            /// </summary>
            /// <param name="n"></param>
            /// <returns></returns>
            public uint reverseBits(uint n)
            {
                BitArray bitArray = new BitArray(BitConverter.GetBytes(n));
                BitArray resultArray = new BitArray(32);
                int j = 0;
                for (var i = bitArray.Length - 1; i >= 0; i--)
                {
                    resultArray[j++] = bitArray[i];
                }
                int[] array = new int[1];
                resultArray.CopyTo(array, 0);
                return (uint)array[0];
            }

        }

        ITestOutputHelper output;

        public 颠倒二进制位(ITestOutputHelper output)
        {
            this.output = output;
        }


        [Fact]
        public void Test1()
        {
            Solution solution = new Solution();
            output.WriteLine("" + solution.reverseBits(0b00000010100101000001111010011100));
            output.WriteLine("" + solution.reverseBits(0b11111111111111111111111111111101));
            output.WriteLine("");
        }
    }
}
