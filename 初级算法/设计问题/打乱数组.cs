﻿using System;
using System.Collections.Generic;
using Xunit;
using Xunit.Abstractions;

namespace 初级算法.设计问题
{
    public class 打乱数组
    {
        /// <summary>
        /// https://leetcode.cn/leetbook/read/top-interview-questions-easy/xn6gq1/
        /// 2021-8-23 双栈存储 当前栈的最小值
        /// 执行用时：312  ms, 在所有 C# 提交中击败了91.67%的用户
        /// 内存消耗：50.6 MB, 在所有 C# 提交中击败了20.00%的用户
        /// </summary>
        public class Solution
        {
            int[] nums;
            Random ra = new Random();
            int len;
            public Solution(int[] nums)
            {
                this.nums = nums;
                len = nums.Length;
            }

            /** Resets the array to its original configuration and return it. */
            public int[] Reset()
            {
                return nums;
            }

            /** Returns a random shuffling of the array. */
            public int[] Shuffle()
            {
                int[] copyNums = new int[len];
                nums.CopyTo(copyNums, 0);
                for (int j = 0; j < len - 1; j++)
                {
                    int x = copyNums[j];
                    int raVal = ra.Next(j, len);
                    copyNums[j] = copyNums[raVal];
                    copyNums[raVal] = x;
                }
                return copyNums;
            }
        }
        ITestOutputHelper output;

        public 打乱数组(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Test1()
        {
            var nums = new int[] { 1, 2, 3 };
            Solution obj = new Solution(nums);
            var s = obj.Shuffle();
            foreach (var item in s)
            {
                output.WriteLine("" + item);
            }
            output.WriteLine("");

            //s = obj.Reset();
            //foreach (var item in s)
            //{
            //    output.WriteLine("" + item);
            //}
            s = obj.Shuffle();

            foreach (var item in s)
            {
                output.WriteLine("" + item);
            }
        }
    }
}
