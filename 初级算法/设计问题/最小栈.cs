﻿using System;
using System.Collections.Generic;
using Xunit;
using Xunit.Abstractions;

namespace 初级算法.设计问题
{
    public class 最小栈
    {
        /// <summary>
        /// https://leetcode.cn/leetbook/read/top-interview-questions-easy/xnkq37/
        /// 2021-8-23 双栈存储 当前栈的最小值
        /// 执行用时：116 ms, 在所有 C# 提交中击败了99.05%的用户
        /// 内存消耗：34.8 MB, 在所有 C# 提交中击败了67.14%的用户
        /// </summary>
        public class MinStack
        {
            Stack<int> stackVal = new Stack<int>();
            Stack<int> stackMin = new Stack<int>();


            /** initialize your data structure here. */
            public MinStack()
            {

            }

            public void Push(int val)
            {
                stackVal.Push(val);
                if (stackMin.Count == 0 || val <= GetMin())
                {
                    stackMin.Push(val);
                }
            }

            public void Pop()
            {
                if (stackVal.Pop() == GetMin())
                {
                    stackMin.Pop();
                }
            }

            public int Top()
            {
                return stackVal.Peek();
            }

            public int GetMin()
            {
                return stackMin.Peek();
            }
        }

        ITestOutputHelper output;

        public 最小栈(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Test1()
        {
            MinStack obj = new MinStack();
            obj.Push(-2);
            obj.Push(0);
            obj.Push(-3);
            var min = obj.GetMin();
            output.WriteLine("" + min);
            obj.Pop();
            int param_3 = obj.Top();
            output.WriteLine("" + param_3);
            int param_4 = obj.GetMin();
            output.WriteLine("" + param_4);


            MinStack obj2 = new MinStack();
            obj2.Push(0);
            obj2.Push(1);
            obj2.Push(0);
            output.WriteLine("" + obj2.GetMin());
            obj2.Pop();
            output.WriteLine("" + obj2.GetMin());
        }
    }
}
