﻿using System.Text;
using Xunit;
using Xunit.Abstractions;

namespace 初级算法.其他
{
    public class 外观数列
    {
        /// <summary>
        /// https://leetcode.cn/problems/count-and-say/
        /// </summary>
        public class Solution
        {
            /*
                n-5  111221
                n-6  312211
                N-7  13112221
           */
            /// <summary>
            /// 2021-7-7 00:59
            /// 执行用时：72 ms, 在所有 C# 提交中击败了100.00%的用户
            /// 内存消耗：25 MB, 在所有 C# 提交中击败了66.19%的用户
            /// </summary>
            /// <param name="n"></param>
            /// <returns></returns>
            public string CountAndSay(int n)
            {
                string x = "1";
                if (n == 1) return x;

                for (int z = 0; z < n - 1; z++)
                {
                    StringBuilder ss = new StringBuilder();
                    for (int i = 0; i < x.Length; i++)
                    {
                        int t = x[i];
                        int num = 0;
                        while (i < x.Length && x[i] == t)
                        {
                            ++num;
                            ++i;
                        }

                        ss.Append(num.ToString());
                        ss.Append(t - '0');
                        --i;
                    }
                    x = ss.ToString();
                }
                return x;
            }
        }
        ITestOutputHelper output;

        public 外观数列(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Test1()
        {

            Solution S = new Solution();
            string r;

            r = S.CountAndSay(1);

            output.WriteLine("" + r);

            r = S.CountAndSay(5);

            output.WriteLine("" + r);
        }
    }
}
