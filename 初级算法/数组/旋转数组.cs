﻿using System;
using Xunit;
using Xunit.Abstractions;

namespace 初级算法.其他
{
    public class 旋转数组
    {
        /// <summary>
        /// https://leetcode.cn/leetbook/read/top-interview-questions-easy/x2skh7/
        /// </summary>
        public class Solution
        {
            public void Rotate(int[] nums, int k)
            {
                if (k == 0) return;

                for (int i = 0; i < k; i++)
                {
                    int temp = nums[nums.Length - 1];
                    for (int j = nums.Length - 1; j > 0; j--)
                    {
                        nums[j] = nums[j - 1];
                    }
                    nums[0] = temp;
                }
            }

            public void Rotate1(int[] nums, int k)
            {
                if (k == 0) return;

                int[] result = new int[nums.Length];
                for (int i = 0; i < nums.Length; i++)
                {
                    result[i] = nums[i];
                }

                for (int i = 0; i < nums.Length; i++)
                {
                    nums[(i + k) % nums.Length] = result[i];
                }
            }
            public int MySqrt(int x)
            {
                return (int)Math.Sqrt(x);
            }
        }
        ITestOutputHelper output;

        public 旋转数组(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Test1()
        {
            Solution S = new Solution();
            int[] r = new int[] { 1, 2, 3, 4, 5, 6, 7 };
            S.Rotate1(r, 3);
            foreach (var item in r)
            {
                output.WriteLine("" + item);
            }
            output.WriteLine("");
            r = new int[] { -1, -100, 3, 99 };
            S.Rotate1(r, 2);
            foreach (var item in r)
            {
                output.WriteLine("" + item);
            }
            output.WriteLine("");
        }
    }
}
