﻿using System.Linq;
using Xunit;
using Xunit.Abstractions;

namespace 初级算法.其他
{
    public class 旋转图像
    {
        public class Solution
        {
            /// <summary>
            /// 时间复杂度：n^2，空间复杂度：n^2
            /// 2021/07/14 21:50
            /// 执行用时：220 ms, 在所有 C# 提交中击败了99.60%的用户
            /// 内存消耗：30.5 MB, 在所有 C# 提交中击败了18.40%的用户
            /// </summary>
            /// <param name="matrix"></param>
            public void Rotate(int[][] matrix)
            {
                int n = matrix.Length;
                int[][] newMatrix = new int[n][];

                for (int i = 0; i < n; i++)
                {
                    newMatrix[i] = new int[n];
                }

                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j < n; j++)
                    {
                        newMatrix[j][n - 1 - i] = matrix[i][j];
                    }
                }

                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j < n; j++)
                    {
                        matrix[i][j] = newMatrix[i][j];
                    }
                }
            }


        }
        ITestOutputHelper output;

        public 旋转图像(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Test1()
        {
            Solution solution = new Solution();
            var matrix = new int[][] {
                    new int[]{1,2,3},
                    new int[]{4,5,6},
                    new int[]{7,8,9}
            };
            solution.Rotate(matrix);
            foreach (var items in matrix)
            {
                output.WriteLine(string.Join(',', items.ToList()) + " ");
            }

            output.WriteLine("");
            matrix = new int[][]
            {
                new int[]{5,1,9,11},
                new int[]{2,4,8,10},
                new int[]{13,3,6,7},
                new int[]{15,14,12,16},
            };
            solution.Rotate(matrix);
            foreach (var a in matrix)
            {
                output.WriteLine(string.Join(',', a.ToList()) + " ");
            }

            output.WriteLine("");
            matrix = new int[][]
            {
                new int[]{1},
            };
            solution.Rotate(matrix);
            foreach (var a in matrix)
            {
                output.WriteLine(string.Join(',', a.ToList()) + " ");
            }

            output.WriteLine("");
            matrix = new int[][]
            {
                new int[]{1,2},
                new int[]{3,4},
            };
            solution.Rotate(matrix);
            foreach (var a in matrix)
            {
                output.WriteLine(string.Join(',', a.ToList()) + " ");
            }
        }
    }
}
