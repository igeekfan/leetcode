﻿using System.Collections.Generic;
using Xunit;
using Xunit.Abstractions;

namespace 初级算法.其他
{
    public class 存在重复元素
    {
        public class Solution
        {
            public bool ContainsDuplicate(int[] nums)
            {
                Dictionary<int, int> keyValuePairs = new Dictionary<int, int>();
                for (int i = 0; i < nums.Length; i++)
                {
                    if (keyValuePairs.ContainsKey(nums[i]))
                    {
                        return true;
                    }
                    keyValuePairs[nums[i]] = 1;
                }
                return false;
            }
        }
        ITestOutputHelper output;

        public 存在重复元素(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Test1()
        {

            Solution S = new Solution();
            var r = S.ContainsDuplicate(new int[] { 1, 2, 3, 1 });

            output.WriteLine("" + r);
            r = S.ContainsDuplicate(new int[] { 1, 2, 3, 4 });
            output.WriteLine("" + r);

            r = S.ContainsDuplicate(new int[] { 1, 1, 1, 3, 3, 4, 3, 2, 4, 2 });
            output.WriteLine("" + r);
        }
    }
}
