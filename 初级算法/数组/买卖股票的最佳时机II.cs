﻿using System.Collections.Generic;
using Xunit;
using Xunit.Abstractions;

namespace 初级算法.其他
{
    public class 买卖股票的最佳时机II
    {
        /// <summary>
        /// https://leetcode.cn/leetbook/read/top-interview-questions-easy/x2zsx1/
        /// </summary>
        public class Solution
        {
            /// <summary>
            /// 贪心算法：算出最赚钱的方式，i+n天-i天赚的钱
            /// 执行用时：92 ms, 在所有 C# 提交中击败了97.81%的用户
            /// 内存消耗：25.1 MB, 在所有 C# 提交中击败了85.38%
            /// </summary>
            /// <param name="prices"></param>
            /// <returns></returns>
            public int MaxProfit(int[] prices)
            {
                if (prices == null || prices.Length < 2) { return 0; }

                int allPrices = 0;
                for(var i = 1; i < prices.Length; i++)
                {
                    int temp=prices[i] - prices[i - 1];
                    if (temp > 0)
                    {
                        allPrices += temp;
                    }
                }
                return allPrices;
            }
        }
        ITestOutputHelper output;

        public 买卖股票的最佳时机II(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Test1()
        {

            Solution S = new Solution();
            var r = S.MaxProfit(new int[] { 7, 1, 5, 3, 6, 4 });

            output.WriteLine("" + r);
            r = S.MaxProfit(new int[] { 1, 2, 3, 4, 5 });
            output.WriteLine("" + r);

            r = S.MaxProfit(new int[] { 7, 6, 4, 3, 1 });
            output.WriteLine("" + r);
        }
    }
}
