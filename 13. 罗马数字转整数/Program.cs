﻿using System;
using System.Collections.Generic;

namespace _13._罗马数字转整数
{
    /// <summary>
    /// https://leetcode.cn/problems/roman-to-integer/
    /// </summary>
    public class Solution
    {
        Dictionary<char, int> dict = new Dictionary<char, int>
            {
                {'M',1000 },
                {'D',500 },
                {'C',100 },
                {'L',50 },
                {'X',10 },
                {'V',5 },
                {'I',1 },
            };
        public int RomanToInt(string s)
        {
            int ans = 0;

            for (var i = 0; i < s.Length; i++)
            {
                int value = dict[s[i]];

                if (i < s.Length - 1 && value < dict[s[i + 1]])
                {
                    ans -= value;
                }
                else
                {
                    ans += value;
                }

            }

            return ans;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(new Solution().RomanToInt("III"));
            Console.WriteLine(new Solution().RomanToInt("IV"));
            Console.WriteLine(new Solution().RomanToInt("IX"));
            Console.WriteLine(new Solution().RomanToInt("LVIII"));
        }
    }
}
