﻿using System;
using System.Linq;

namespace _26._删除有序数组中的重复项
{
    /// <summary>
    /// https://leetcode.cn/problems/remove-duplicates-from-sorted-array/
    /// </summary>
    class Program
    {
        public class Solution
        {
            /// <summary>
            /// 通过双指针移动
            /// </summary>
            /// <param name="nums"></param>
            /// <returns></returns>
            public int RemoveDuplicates(int[] nums)
            {
                int count = nums.Length;

                if (count == 0)
                {
                    return 0;
                }

                int fast = 1, slow = 1;

                while (fast < count)
                {
                    if (nums[fast] != nums[fast - 1])
                    {
                        nums[slow] = nums[fast];
                        slow++;
                    }

                    fast++;
                }


                return slow;
            }

            //直接使用Distinct 转换，不过将distinctNums赋值给nums，nums不会变化
            public int RemoveDuplicates2(int[] nums)
            {
                int count = nums.Length;

                if (count == 0)
                {
                    return 0;
                }

                var distinctNums = nums.Distinct().ToList();

                var i = 0;
                foreach (var item in distinctNums)
                {
                    nums[i++] = item;
                }

                return nums.Length;
            }
        }
        static void Main(string[] args)
        {
            int[] nums = new int[] { 1, 1, 2 };
            //new Solution().RemoveDuplicates(nums);

            new Solution().RemoveDuplicates2(nums);

            for (int i = 0; i < nums.Count(); i++)
            {
                Console.WriteLine(nums[i]);

            }
        }
    }
}
